# 4. Auftrag

## Netzwerkplan

![](./images/1.jpg)

## Auftrag

- Sie gehen strukturiert vor, um die Fehler zu finden und zu korrigieren.
- Ihre ausgeführten Schritte für die Fehlersuche halten Sie in Ihrer Dokumentation fest.
- Zusätzlich halten Sie die gefundenen Fehler und deren Behebung in der separaten Tabelle fest.
- Nachdem Sie sämtliche Fehler gefunden haben, testen Sie Ihr Lösungsfile mit dem Coach/Lehrer (Live-Demo)

## Gefundene Fehler und Korrekturen

| Gerät | Gefundener Fehler, ausgeführte Korrektur                                                               |
| ----- | ------------------------------------------------------------------------------------------------------ |
| RT-01 | IP-Adresse vom Router Port im vierten Subnetz ist falsch. Angepasst auf 170.11.83.25                   |
| RT-01 | Subnetzmaske vom Router Port im 7. Subnetz Im dritten Oktett ist falsch. Angepasst auf 255.255.255.248 |
| PC-2  | IP-Adresse vom Gateway ist falsch im dritten Oktett. Angepasst auf 170.11.83.1                         |
| PC-10 | IP-Adresse ist falsch im vierten Oktett. Angepasst auf 170.11.83.10                                    |
| PC-10 | IP-Adresse vom Gateway ist falsch im vierten Oktett. Angepasst auf 170.11.83.9                         |
| PC-26 | Subnetzmaske ist falsch im zweiten Oktett. Angepasst auf 255.255.255.248                               |
| PC-27 | IP-Adresse vom Gateway ist falsch im ersten Oktett. Angepasst auf 170.11.83.25                         |
| PC-43 | IP-Adresse vom Gateway ist leer. Angepasst auf 170.11.83.41                                            |
| PC-50 | IP-Adresse ist falsch im vierten Oktett. Angepasst auf 170.11.83.50                                    |
| PC-51 | Subnetzmaske ist falsch im dritten und vierten Oktett. Angepasst auf 255.255.255.248                   |
| PC-59 | IP-Adresse vom Gateway ist falsch im vierten Oktett. Angepasst auf 170.11.83.57                        |

## Beschreibung

### Excel-Sheet Vorgehen

Um 4 gleich grosse Subnetze zu erstellen wird das Kuchennetzstück durch 8 geteilt beginnend bei /26

/27 -> 2 gleich grosse Subnetze

/28 -> 4 gleich grosse Subnetze

/29 -> 8 gleich grosse Subnetze

Zuerst habe ich die neue Netzmaske in der Tabelle dargestellt. Daraufhin habe ich die Netzwerk-ID in der Tabelle dargestellt und die schwarzen Striche für das /29er Sunbetz gezogen. Danach konnte ich ganz einfach die First IP, Last IP und den Broadcast ergänzen.

Da das Netz bei /26 beginnt, das würde 4 gleich grosse Kuchnestücke ergeben -> 256/4 = 64. Jetzt wird das vorhandene Netz in gleich grosse 8er Stücke geteilt -> 64/8 = 8. Wir haben also immer 8 mögliche IPs in einem Subnetz (-2 durch die Netz-ID und den Broadcast können wir nur 6 IPs vergeben)

Die Netze sind also wie folgt aufgeteilt:
0-7, 8-15, 16-23, 24-31, 32-39, 40-47, 48-55, 56-64. Der erste und der letzte Wert sind dabei immer Netz-ID oder Broadcast. Mit diesen Informationen konnte ich die 7 anderen Netzwerktabellen ausfüllen.
![](./images/2.jpg)

### Filius-File Vorgehen

1. Schritt: Bei allen Geräten die Subnetzmaske checken, ob diese richtig angegeben wurde
2. Schritt: Beim Router schauen, ob die IPs der Ports in die richtigen Subnetze aufgeteilt worden waren
3. Schritt: bei den Clients überprüfen, ob die richtigen Gateways (IP-Adressen der Ports) angegeben wurden
4. Schritt: IP-Adressen der Clients checken, ob diese in der richtigen Reihenfolge nach dem Router verteilt wurden
   ![](./images/3.jpg)
