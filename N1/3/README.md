# 3. Auftrag

## Netzwerkplan

![](./images/5.jpg)

## Vorgaben

- Vom ISP erhalten sie den IP-Range: 25.30.120.0 /26
- Erstellen Sie vier gleichgrosse Subnetze - für jede Abteilung eines.
- Die erste IP-Adresse jedes Subnets ist für den Router reserviert.
- Die Endgeräte erhalten die nächsthöheren IP-Adressen
- Namenskonvention (gemäss Netzwerkplan)

## Beschreibung

### Excel-Sheet Vorgehen

Um 4 gleich grosse Subnetze zu erstellen wird das Kuchennetzstück durch 4 geteilt beginnend bei /26

/27 -> 2 gleich grosse Subnetze

/28 -> 4 gleich grosse Subnetze

Ich habe die neue Netzwerkmasek eingetragen .Daraufhin habe ich die Netzwerk-ID in der Tabelle dargestellt und die Schwarzen Striche für das /28er Sunbetz gezogen. Danach konnte ich ganz einfach die First IP, Last IP und den Broadcast ergänzen.

Da das Netz bei /26 beginnt, das würde 4 gleich grosse Kuchnestücke ergeben -> 256/4 = 64. Jetzt wird das vorhandene Netz in gleich grosse 4er Stücke geteilt -> 64/4 = 16. Wir haben also immer 16 mögliche IPs in einem Subnetz (-2 durch die Netz-ID und den Broadcast können wir nur 14 IPs vergeben)

Die Netze sind also wie folgt aufgeteilt:
0-15, 16-31, 32-47, 48-63. Der erste und der letzte Wert sind dabei immer Netz-ID oder Broadcast. Mit diesen Informationen konnte ich die 3 letzten Netzwerktabellen ausfüllen.
![](./images/1.jpg)
![](./images/2.jpg)

### Filius-File Vorgehen

1. Schritt: bei allen Geräten die Subnetzmaske 255.255.255.240 ergänzen
2. Schritt: beim Router die richtigen Ports mit den passenden IPs versehen -> 25.30.120.1, 25.30.120.17, 25.30.120.33, 25.30.120.49
3. Schritt: bei allen Geräten die IPs hinzufügen
   z.B. Pc-18 bekommt die 25.30.120.18 da diese IP-Adresse genau im Sunbetz vorhanden ist...
   ![](./images/3.jpg)

Schlussendlich habe ich noch einige Tests mit der Befehlszeile vom Pc-2 durchgeführt, indem ich zum Beispiel Pc-50 und Pc-35 angepingt habe. Wenn es dann kein Paketverlust gab, war die Installation erfolgreich.
![](./images/4.jpg)
