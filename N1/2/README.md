# 2. Auftrag 
## Netzwerkplan
![](./images/4.jpg)

## Vorgaben
- Vom ISP erhalten sie den IP-Range: 59.136.34.0 /24
- Erstellen Sie acht gleichgrosse Subnetze - für jede Abteilung eines.

## Beschreibung
### Excel-Sheet Vorgehen
Um 4 gleich grosse Subnetze zu erstellen wird das Kuchennetzstück durch 4 geteilt

/25 -> 2 gleich grosse Subnetze 

/26 -> 4 gleich grosse Subnetze

/27 -> 8 gleich grosse Subnetze

Zuerst habe ich die neue Netzmaske in der Tabelle dargestellt. Daraufhin habe ich die Netzwerk-ID in der Tabelle dargestellt und den Schwarzen Strich für das /27er Sunbetz gezogen. Danach konnte ich ganz einfach die First IP, Last IP und den Broadcast ergänzen.  

Da das Netz in gleich grosse 8er Stücke geteilt wird kann man das letzte Oktett durch 8 teilen 256/8 = 32. Wir haben also immer 32 mögliche IPs in einem Subnetz (-2 durch die Netz-ID und den Broadcast können wir nur 30 IPs vergeben)

Die Netze sind also wie folgt aufgeteilt:
0-31, 32-63, 64-95, 96-127, 128-159, 160-191, 192-223, 224-255. Der erste und der letzte Wert sind dabei immer Netz-ID oder Broadcast. Mit diesen Informationen konnte ich die 7 anderen Netzwerktabellen ausfüllen.
![](./images/1.jpg)

### Filius-File Vorgehen
1. Schritt: bei allen Geräten die Subnetzmaske 255.255.255.224 ergänzen
2. Schritt: beim Router die richtigen Ports mit den passenden IPs versehen -> 59.136.34.1, 59.136.34.33, ...
3. Schritt:
4. Schritt: bei allen Geräten die IPs hinzufügen 
z.B. Pc-40 bekommt die IP-Adresse 59.136.34.40, weil es in der Range liegt und weil es so übersichtlicher wird.
![](./images/2.jpg)

Schlussendlich habe ich noch einige Tests mit der Befehlszeile vom PC-10 durchgeführt, indem ich zum Beispiel PC-130 und PC-210 angepingt habe. Wenn es dann kein Paketverlust gab, war die Installation erfolgreich.
![](./images/3.jpg)