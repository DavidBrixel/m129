# 1. Auftrag

## Netzwerkplan

![](./images/1.jpg)

## Vorgaben

- Vom ISP erhalten sie den IP-Range: 133.95.48.0 /23
- Erstellen Sie vier gleichgrosse Subnetze - für jede Abteilung eines.

## Beschreibung

### Excel-Sheet Vorgehen

Um 4 gleich grosse Subnetze zu erstellen wird das Kuchennetzstück durch 4 geteilt beginnend bei /23

/24 -> 2 gleich grosse Subnetze

/25 -> 4 gleich grosse Subnetze

Zuerst habe ich die neue Netzmaske in der Tabelle dargestellt. Daraufhin habe ich die Netzwerk-ID in der Tabelle dargestellt und die schwarzen Striche für das /25er Sunbetz gezogen. Danach konnte ich ganz einfach die First IP, Last IP und den Broadcast ergänzen.

Da das Netz in 4 gleich grosse Stücke geteilt wird haben wir folgende Subnetze 133.95.48.0, 133.95.48.128, 133.95.49.0, 133.95.49.128. Wir haben also immer 128 mögliche IPs in einem Subnetz (-2, denn durch die Netz-ID und den Broadcast können wir nur 126 IPs vergeben)

Mit diesen Informationen konnte ich die letzten 3 Netzwerktabellen ausfüllen.
![](./images/2.jpg)

### Cisco Packet Tracer File Vorgehen

1. Schritt: alle Geräte mit Kabel verbinden
2. Schritt: bei allen Geräten die Subnetzmaske 255.255.255.128 ergänzen
3. Schritt: beim Router die richtigen Ports mit den passenden IPs versehen -> 133.95.48.1, 133.95.48.129, 133.95.49.1, 133.95.49.129
4. Schritt: bei allen Clients die richtigen IP-Adressen der Gateways eintragen
5. Schritt: bei allen Geräten die IPs hinzufügen
   z.B. Pc-8050 bekommt im letzten Oktett .50,
   Pc-8140 bekommt daher 133.95.49.140, ...
   ![](./images/3.jpg)

Danach habe ich die Konsole von PC-8050 geöffnet und zwei geräte aus einem anderen Subnetz angepingt(z.B. PC-8410 und PC-9410), zur Überprufung, um sicher zugehen, dass die Einstellungen der Clients und des Gateway/Router richtig sind.
![](./images/4.jpg)

## Informationen

- in jeder Abteilung gibt es nun noch 123 freie IP-Adressen
- die Firma kann daher ohne Probleme wachsen, bis sie in einer Abteilung mehr als 123 weitere IP-Adressen vergeben haben
- Das Netz Design macht grundsätzlich schon Sinn, vielleicht wenn die einzelnen Abteilungen in getrennten Gebäuden arbeiten, könnte man jeder Abteilung ihren eigenen Router zuweisen. Man bräuchte dann das Routing
- Man könnte das Netzwerk noch ein wenig anpassen, wenn man nicht 123 freie IP-Adressen braucht in der Abteilung, oder die Firma nicht schnell wächst, kann man die Subnetze verkleinern und somit könnte noch eine 5. Abteilung ein Subnetz bekommen, oder man sparrte einfach Platz im Subnetz
- Um eine Optimierung vom Netzwerk vorzutreffen, wäre es wichtig zu wissen, wie schnell oder langsam die Firma wächst und ob die einzelnen Abteilungen in getrennten Gebäuden arbeiten
