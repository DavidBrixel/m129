# 3. Auftrag

## Netzwerkplan

![](./images/1.jpg)

## Vorgaben

- Vom ISP erhalten sie den IP-Range: 34.112.98.0 /24
- Erstellen Sie unterschiedlich grosse Subnetze - für jede Abteilung eines, plus noch das Transfernetz.

## Beschreibung

| Produkt:  | Produktion | Verkauf |
| :-------- | :--------: | :-----: |
| PCs       |     15     |    5    |
| Laptops   |     10     |    6    |
| Drucker   |     10     |    2    |
| **Total** |     35     |   13    |

mithilfe dieser Tabelle kann man ganz einfach ablesen, welche grösse der Sunbetze für die einzelnen Abteilungen geeignet sind.

Produktion -> benötigt ein /26er Netz, da ein /27er Netz mit 30 verfügbaren Host IP-Adressen nicht reichen würde

Verkauf -> benötigt ein /28er Netz, welches perfekt aufgeht mit 13 Geräten und einem Router

Transfernetz (2 Router) -> benötigt ein /30er Netz, passt perfekt mit 2 Routern, 1 Broadcast und 1 Netz-ID

### Excel-Sheet Vorgehen

Um diese Aufgabe bewältigen zu können hat mir die Vorlage von Herrn Callisto sehr geholfen.

Ich habe mich ausserdem dazu entschieden das Transfernetz am Schluss anzufügen, da eine Vergrösserung des Subnetzes Verkauf sowieso in Betracht gezogen werden müsste, und man so das Transfernetz nicht nochmals umplanen müsste.

Zuerst muss man darauf achten, dass man die Subnetze der Grösse nach im Kreis anordnet (Poduktion > Verkauf > Transfernetz) und dort die Grenezen der Netze richtig setzt
Produktion 0-63, Verkauf 64-79, Transfernetz 252-255
![](./images/2.jpg)

Aufgrund der Informationen (grösse der Netze, wann sie beginnen und wann sie enden) der obigen Tabelle und den Kreis-Diagrammen konnte ich die Excel-Tabellen ausfüllen.
![](./images/3.jpg)

### Cisco Packet Tracer File Vorgehen

1. Schritt: bei den Geräten im Subnetz von Produktion Subnetzmaske 255.255.255.192, Verkauf Subnetzmaske 255.255.255.240 ergänzen
2. Schritt: bei den Router Ports vom Transfer Netz die Subnetzmaske 255.255.255.252 einfügen
3. Schritt: beim Router Ports in den jeweiligen Subnetzen mit den passenden IPs versehen -> 34.112.98.1, 34.112.98.65, (34.112.98.253 beim ISP Router) und 34.112.98.254
4. Schritt: bei allen Clients die richtigen IP-Adressen der Gateways eintragen
5. Schritt: bei allen Geräten die IPs hinzufügen
   z.B. PC-P01 bekommt im letzten Oktett .2, da das die nächst verfügbare IP-Adresse ist nach dem Router,
   daher 34.112.98.2, ...
6. Schritt: jetzt muss man beim ISP Router das vorgegebene Netzwerk mit der Netzmaske /23 eintragen und den nächsten Hop also die IP vom Router 1 (34.112.98.254)
7. Schritt: beim Router 1 muss man das Netz 0.0.0.0 und die Maske 0.0.0.0 also /0 eintragen und den nächsten Hop also die IP vom ISP Router (34.112.98.253)
   ![](./images/4.jpg)

Danach habe ich die Konsole von PC-P01 geöffnet und zwei geräte aus einem anderen Subnetz angepingt(z.B. PC-V01, PC-V02 und den ISP Router), zur Überprufung, um sicher zugehen, dass die Einstellungen der Clients und des Gateway/Router richtig sind.
![](./images/5.jpg)

## Informationen

- freie IP-Adressen pro Abteilung: Produktion -> 26, Verkauf -> 0
- die Firma kann nicht wirklich wachsen, da im Verkauf keine IP-Adressen mehr vergeben werden können
- Das Netz Design macht grundsätzlich schon Sinn abgesehen von der Wachstums Möglichkeit, vielleicht wenn die einzelnen Abteilungen in getrennten Gebäuden arbeiten, könnte man jeder Abteilung ihren eigenen Router zuweisen. Man bräuchte dann das Routing
- Man könnte das Netzwerk noch ein wenig anpassen, z.B. das Subnetz vom Verkauf muss unbedingt vergrössert werden auf ein /27er Subnetz oder ein /26er wenn die Abteilung sehr schnell wächst
- Um eine Optimierung vom Netzwerk vorzutreffen, wäre es wichtig zu wissen, wie schnell oder langsam die Firma wächst und ob die einzelnen Abteilungen in getrennten Gebäuden arbeiten
