# 4. Auftrag

## Netzwerkplan

![](./images/1.jpg)

## Vorgaben

- Vom ISP erhalten sie den IP-Range: 219.60.30.0 /23
- Erstellen Sie unterschiedlich grosse Subnetze - für jede Abteilung eines, plus noch das Transfernetz (insgesamt 5).

## Beschreibung

| Produkt:  | GL  | Betrieb | Verkauf | Einkauf |
| :-------- | :-: | :-----: | :-----: | :-----: |
| PCs       |  3  |   44    |    6    |   29    |
| Laptops   |  3  |   36    |   10    |   30    |
| Drucker   |  2  |    5    |    4    |    6    |
| **Total** |  8  |   85    |   20    |   65    |

mithilfe dieser Tabelle kann man ganz einfach ablesen, welche grösse der Sunbetze für die einzelnen Abteilungen geeignet sind.

GL -> benötigt ein /28er Netz, da in einem /29er Netz kein Platz mehr für Broadcast und Netz-ID wäre

Betrieb -> benötigt ein /25er Netz, in welchen man 126 IP-Adressen vergeben kann

Verkauf -> benötigt ein /27er Netz, in welchen man 30 IP-Adressen vergeben kann

Einfkauf -> benötigt ein /25er Netz, in welchen man 126 IP-Adressen vergeben kann

Transfernetz (2 Router) -> benötigt ein /30er Netz, passt perfekt mit 2 Routern, 1 Broadcast und 1 Netz-ID

### Excel-Sheet Vorgehen

Um diese Aufgabe bewältigen zu können hat mir die Vorlage von Herrn Callisto sehr geholfen.

Wie man an diesem Beispiel sieht sind heir anstatt normaler Weise 1 Kreis 2 Kreise vorhanden. Es ist also ein /23er ISP Netz zur Verfügung gestellt worden.

Man kann nun die zwei grössten Abteilungen in den ersten Kreis unterbringen (Betrieb mit 85 benötigten IP-Adressen, welche ein /25er Sunbetz benötigt) und (Einkauf mit 65 benötigten IP-Adressen, welche auch ein /25er Sunbetz benötigt)

im zweiten Kreis kann man die restlichen zwei Abteilungen + das Transfernetz speichern
(GL braucht mit 8 benötigten IP-Adressen ein /28er Subnetz, da noch Broadcast und Netz-ID dazu kommen kann man kein /29er Sunbetz brauchen), (Verkauf braucht mit 20 benötigten IP-Adressen ein /27er Subnetz) und (Transfernetz braucht mit 2 benötigten IP-Adressen ein /30er Subnetz, da noch Broadcast und Netz-ID dazu kommen und es so perfekt aufgeht)
![](./images/3.jpg)

Aufgrund der Informationen (grösse der Netze, wann sie beginnen und wann sie enden) der obigen Tabelle und den Kreis-Diagrammen konnte ich die Excel-Tabellen ausfüllen.
![](./images/2.jpg)

### Cisco Packet Tracer File Vorgehen

1. Schritt: alle Geräte mit Kabel verbinden
2. Schritt: bei den Geräten im Subnetz von Einkauf oder Betrieb Subnetzmaske 255.255.255.128 ergänzen
3. Schritt: bei den Geräten im Subnetz von Verkauf die Subnetzmaske 255.255.255.224, bei allen Geräten von GL die Subnetzmaske 255.255.255.240 und bei Transfernetz Router Ports 255.255.255.252 ergänzt.
4. Schritt: beim Router die richtigen Ports mit den passenden IPs versehen -> 219.60.30.1, 219.60.30.129, 219.60.31.1, 219.60.31.33, 219.60.31.254 (219.60.31.253 beim ISP Router)
5. Schritt: bei allen Clients die richtigen IP-Adressen der Gateways eintragen
6. Schritt: bei allen Geräten die IPs hinzufügen
   z.B. PC-G034 bekommt im letzten Oktett .34,
   daher 219.60.31.34, ...
7. Schritt: jetzt muss man beim ISP Router das vorgegebene Netzwerk mit der Netzmaske /23 eintragen und den nächsten Hop also die IP vom Router 1 (219.60.31.254)
8. Schritt: beim Router 1 muss man das Netz 0.0.0.0 und die Maske 0.0.0.0 also /0 eintragen und den nächsten Hop also die IP vom ISP Router (219.60.31.253)
   ![](./images/4.jpg)

Danach habe ich die Konsole von PC-G034 geöffnet und zwei geräte aus einem anderen Subnetz angepingt(z.B. PC-B002, PC-V002 und den ISP Router), zur Überprufung, um sicher zugehen, dass die Einstellungen der Clients und des Gateway/Router richtig sind.
![](./images/5.jpg)

## Informationen

- freie IP-Adressen pro Abteilung: GL -> 5, Verkauf -> 9, Betrieb -> 40, Einkauf -> 60
- die Firma kann daher ohne Probleme wachsen, bis sie in einer Abteilung mehr IP-Adressen als vorhandene vergeben müssten
- Das Netz Design macht grundsätzlich schon Sinn, vielleicht wenn die einzelnen Abteilungen in getrennten Gebäuden arbeiten, könnte man jeder Abteilung ihren eigenen Router zuweisen. Man bräuchte dann das Routing
- Man könnte das Netzwerk noch ein wenig anpassen, wenn man zum Beispiel wüsste, dass die Firma in bestimmten Abteilungen schnell wächst, könnte man für diese Abteilung das Subnetz vergrössern
- Um eine Optimierung vom Netzwerk vorzutreffen, wäre es wichtig zu wissen, wie schnell oder langsam die Firma wächst und ob die einzelnen Abteilungen in getrennten Gebäuden arbeiten
