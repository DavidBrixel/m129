# 4. Auftrag

## Netzwerkplan

![](./images/1.jpg)

## Vorgaben

- Vom ISP erhalten sie den IP-Range: 178.19.22.0 /24
- Erstellen Sie unterschiedlich grosse Subnetze - für jede Abteilung eines, plus noch die Transfernetze.

## Beschreibung

| Produkt:  | Site **Global** | Site **Local** |
| :-------- | :-------------: | :------------: |
| PCs       |       30        |       8        |
| Laptops   |       60        |       7        |
| Drucker   |        7        |       2        |
| **Total** |     **97**      |     **17**     |

mithilfe dieser Tabelle kann man ganz einfach ablesen, welche grösse der Sunbetze für die einzelnen Abteilungen geeignet sind.

### Netzwerk Design

### Netzgrössen

Global -> benötigt ein /25er Netz, da 100 Clients nicht in ein /26er Netz rein passen würden

Local -> ein /26er Netz wird verwendet, obwohl man das ganze in ein /27er Netz unterbringen könnte, aber ich habe ein mögliches Wachstum beachtet

Transfernetz I und T (2 Router) -> benötigt ein /30er Netz, passt perfekt mit 2 Routern, 1 Broadcast und 1 Netz-ID

| Netz   | Benötigte Adressen                      | Gewählte Netzgrösse               |
| ------ | --------------------------------------- | --------------------------------- |
| Netz I | 0 PCs + 0 LTs + 0 DR + 2 RT + 2 = 4     | 4 (/30)                           |
| Netz T | 0 PCs + 0 LTs + 0 DR + 2 RT + 2 = 4     | 4 (/30)                           |
| Netz A | 30 PCs + 60 LTs + 7 DR + 1 RT + 2 = 100 | 100 (/25 -> 128)                  |
| Netz B | 8 PCs + 7 LTs + 2 DR + 1 RT + 2 = 20    | 20 (/26 -> 64, Wachstum beachtet) |

### Netzadressen

| Netz   | Grösse | Netzadresse/Netzmaske (Bit) | Dezimale Schreibweise der Netzmaske | Broadcastadresse |
| ------ | ------ | --------------------------- | ----------------------------------- | ---------------- |
| Netz I | 4      | 178.19.22.252 /30           | 255.255.255.252                     | 178.19.22.255    |
| Netz T | 4      | 178.19.22.248 /30           | 255.255.255.252                     | 178.19.22.251    |
| Netz A | 100    | 178.19.22.0 /25             | 255.255.255.128                     | 178.19.22.127    |
| Netz B | 20     | 178.19.22.128 /26           | 255.255.255.192                     | 178.19.22.191    |

im /24er Kreis kann man die zuerst das grösste Subnetz speichern /25, dann das zweit grösste /26. Damit haben wir schon drei Viertel des Kreises für die beiden Firmen verbraucht + die zwei /30er Transfernetze am Schluss

Aufgrund der Informationen (grösse der Netze, wann sie beginnen und wann sie enden) der obigen Tabelle konnte ich die Excel-Tabellen ausfüllen.
![](./images/2.jpg)

### Router-Interfaces

| **Router**      | **Interfaceadresse** | **Interface** |
| --------------- | -------------------- | ------------- |
| Router A Netz I | 178.19.22.253 /30    | s1            |
| Router A Netz A | 178.19.22.1 /25      | e0            |
| Router A Netz T | 178.19.22.249 /30    | s0            |
| Router B Netz B | 178.19.22.129 /26    | e0            |
| Router B Netz T | 178.19.22.250 /30    | s0            |

### Routingtabelle Router A

| **Destination Network** (Zielnetz + Netzmaske) | **Next Hop** (Nächster Router auf dem Weg zum Ziel) | **Metric** (hier Hop Count) | **Interface** (auf diesem Router) |
| ---------------------------------------------- | --------------------------------------------------- | --------------------------- | --------------------------------- |
| (A) 178.19.22.0 /25                            | --                                                  | 0                           | e0                                |
| (T) 178.19.22.248 /30                          | --                                                  | 0                           | s0                                |
| (I) 178.19.22.252 /30                          | --                                                  | 0                           | s1                                |
| (B) 178.19.22.128 /26                          | 178.19.22.250 /30                                   | 1                           | s0                                |
| (Default) 0.0.0.0 / 0                          | 178.19.22.254 /30                                   | --                          | s1                                |

### Routingtabelle Router B

| **Destination Network** (Zielnetz + Netzmaske) | **Next Hop** (Nächster Router auf dem Weg zum Ziel) | **Metric** (hier Hop Count) | **Interface** (auf diesem Router) |
| ---------------------------------------------- | --------------------------------------------------- | --------------------------- | --------------------------------- |
| (T) 178.19.22.248 /30                          | --                                                  | 0                           | s0                                |
| (B) 178.19.22.128 /26                          | --                                                  | 0                           | e0                                |
| (Default) 0.0.0.0 / 0                          | 178.19.22.1 /25                                     | --                          | s0                                |

### Cisco Packet Tracer File Vorgehen

1. Schritt: alle Geräte mit Kabel verbinden
2. Schritt: bei den Geräten im Subnetz von Global oder Betrieb Subnetzmaske 255.255.255.128 ergänzen
3. Schritt: bei den Geräten im Subnetz von Local die Subnetzmaske 255.255.255.192 und bei Transfernetz Router Ports 255.255.255.252 ergänzt.
4. Schritt: beim Router die richtigen Ports mit den passenden IPs versehen -> 178.19.22.1, 178.19.22.129, 178.19.22.249, 178.19.22.250, 178.19.22.253 (178.19.22.254 beim ISP Router)
5. Schritt: bei allen Clients die richtigen IP-Adressen der Gateways eintragen
6. Schritt: bei allen Geräten die IPs hinzufügen
7. Schritt: beim Router A in der Routing Tabelle Netzwerkadresse vom Netz B hinzufügen und den default 0.0.0.0
8. Schritt: beim Router B in der Routing Tabelle den Default 0.0.0.0 eintragen
9. Schritt: beim ISP Router in der Routing Tabelle das gegebene Netz vom ISP eintragen
   z.B. PC0 bekommt die nächst verfügbare IP-Adresse nach dem Router
   daher 178.19.22.2, ...
   ![](./images/3.jpg)

Danach habe ich die Konsole von PC0 geöffnet und zwei geräte aus einem anderen Subnetz angepingt(z.B. PC1, Laptop 1 und den ISP Router), zur Überprufung, um sicher zugehen, dass die Einstellungen der Clients und des Gateway/Router richtig sind.
![](./images/4.jpg)

## Informationen

- freie IP-Adressen pro Abteilung: Global -> 28, Local -> 44
- die Firma kann ohne Probleme wachsen, bis sie in einer Abteilung mehr IP-Adressen als vorhandene vergeben müssten
- Man könnte das Netzwerk noch ein wenig anpassen, wenn man zum Beispiel wüsste, dass eine Firma schnell wächst, könnte man für diese Firma das Subnetz vergrössern, jedoch hat man irgendwann keine Kapazitäten mehr, da irgendwann der Netzwerkkucken voll wäre, in diesem Fall müsste man ein /23er Netz vom ISP anfragen, um mehr IP-Adressen verteilen zu können
- Um eine Optimierung vom Netzwerk vorzutreffen, wäre es wichtig zu wissen, wie schnell oder langsam die Firma wächst.
